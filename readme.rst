###################
Job Recruitment
###################

Description will be added here

*******************
Installation Guide
*******************
**Clone repo :** git clone https://unreleased@bitbucket.org/unreleased/job-recruitment.git 

**copy htaccess :**  copy .htaccess.example items to .htaccess

**copy config :** copy /config/config.example to /config/config.php

**copy database :**  copy /config/database.example to /config/database.php

**database import :**  import database from trello board


**************************
Changelog and New Features
**************************

You can find a list of all changes for each release in the `user
guide change log <https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/changelog.rst>`_.

*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

