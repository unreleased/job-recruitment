<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Job Recruitment">
		<meta name="author" content="Job Recruitment">

		<link rel="shortcut icon" href="assets/images/favicon_1.ico">

		<title>Job Recruitment</title>

		<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>

	</head>
	<body>

		<div class="account-pages"></div>
		<div class="clearfix"></div>		
		<div class="wrapper-page">
			<div class="card-box">
				<div class="panel-heading">
					<h4 class="text-center"> Sign In to <strong class="text-custom">JOB REC</strong></h4>
				</div>
				<div class="p-20">
				
				 <?php $this->load->view('auth/session_msg'); ?>

				 <?php if ($this->session->userdata('error_msg')) {
} ?>

					<form action="check_login" method="post" id="loginForm">

						<div class="form-group ">
							<div class="col-12">
								<input class="form-control" name="email" type="email" required="" placeholder="Email">
							</div>
						</div>

						<div class="form-group">
							<div class="col-12">
								<input class="form-control" name="password" type="password" required="" placeholder="Password" minlength="6">
							</div>
						</div>

						<!--<div class="form-group ">
							<div class="col-12">
								<div class="checkbox checkbox-primary">
									<input id="checkbox-signup" type="checkbox">
									<label for="checkbox-signup"> Remember Me </label>
								</div>
							</div>
						</div> -->

						<div class="form-group text-center m-t-40">
							<div class="col-12">
								<button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">
									Log In
								</button>
							</div>
						</div>

						<div class="form-group m-t-20 m-b-0">
							<div class="col-12">
								<a href="forgot_password" class="text-dark"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
							</div>
						</div>
						
						<div class="form-group m-t-20 m-b-0">
							<div class="col-12 text-center">
								<h5 class="font-18"><b>Sign in with</b></h5>
							</div>
						</div>
						
						<div class="form-group m-b-0 text-center">
							<div class="col-12">
								<button type="button" class="btn btn-sm btn-facebook waves-effect waves-light m-t-20">
		                           <i class="fa fa-facebook m-r-5"></i> Facebook
		                        </button>

		                        <button type="button" class="btn btn-sm btn-twitter waves-effect waves-light m-t-20">
		                           <i class="fa fa-twitter m-r-5"></i> Twitter
		                        </button>

		                        <button type="button" class="btn btn-sm btn-googleplus waves-effect waves-light m-t-20">
		                           <i class="fa fa-google-plus m-r-5"></i> Google+
		                        </button>
							</div>
						</div>
					</form>

				</div>
			</div>
			

		</div>

		<script>
			var resizefunc = [];
		</script>

		<!-- jQuery  -->
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/detect.js"></script>
		<script src="assets/js/fastclick.js"></script>
		<script src="assets/js/jquery.slimscroll.js"></script>
		<script src="assets/js/jquery.blockUI.js"></script>
		<script src="assets/js/waves.js"></script>
		<script src="assets/js/wow.min.js"></script>
		<script src="assets/js/jquery.nicescroll.js"></script>
		<script src="assets/js/jquery.scrollTo.min.js"></script>

		<script src="assets/js/jquery.core.js"></script>
		<script src="assets/js/jquery.app.js"></script>		
        <script type="text/javascript" src="plugins/parsleyjs/parsley.min.js"></script>

	</body>
</html>


<script type="text/javascript">
	/*form validation*/

 $(document).ready(function() {
                $('form').parsley();
            });
</script>