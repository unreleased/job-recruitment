<!-- ========== Left Sidebar Start ========== -->

<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>

                <li class="text-muted menu-title">Navigation</li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span> </a>
                </li>                            

                <li class="has_sub">
                    <a href="categories/view" class="waves-effect"> <i class="fa fa-list-alt"></i> <span> Job Categories </span> </a>
                </li>
                <li class="has_sub">
                    <a href="skill/view" class="waves-effect"> <i class="fa fa-th"></i> <span> Skill </span> </a>
                </li>

                <li class="has_sub">
                    <a href="job/location" class="waves-effect"> <i class="fa fa-map-marker"></i> <span> Job Locations </span> </a>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"> <i class="fa fa-id-badge"></i> <span> Jobs </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="job/addJob">Add Job</a></li>
                        <li><a href="#">All Job</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"> <i class="fa fa-users"></i> <span>All Jobs Applications </span> </a>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cogs"></i> <span> Settings </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="#">Admin Profile</a></li>
                        <li><a href="#">Company Settings</a></li>
                        <li><a href="#">User Role & Permission</a></li>
                        <li><a href="#">Language Settings</a></li>
                        <li><a href="#">Theme Settings</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="emailtemplate/view" class="waves-effect"> <i class="fa fa-envelope-o"></i> <span>Email Templates</span> </a>
                </li>                           

            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->
