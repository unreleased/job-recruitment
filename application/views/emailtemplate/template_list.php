

<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b>Email Templates</b></h4>
                        <div class="p-20">
                            <?php $this->load->view('session_msg'); ?>
                            <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <table id="email-template-datatable"
                                                   class="table table-striped table-bordered table-hover table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Template Type</th>
                                                    <th>Subject</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php if ($email_templates) {
    ?>
                                                    <?php foreach ($email_templates as $t) {
        ?>
                                                        <tr>
                                                            <td><?= $t->email_template_type; ?></td>
                                                            <td><?= $t->email_template_subject; ?></td>

                                                            <td> 
                                                                <a title="Edit Template"
                                                                   style="color: #2b2b2b"
                                                                   href="emailtemplate/edit/<?= $t->email_template_id; ?>"
                                                                   class=""><i class="fa fa-pencil-square-o fa-lg"
                                                                               aria-hidden="true"></i>
                                                                </a> 
                                                            </td>
                                                        </tr>
                                                    <?php
    } ?>
                                                <?php
} ?>
                                                </tbody>
                                            </table>                                    
                                </div>
                            </div>
                            <!-- end: page -->

                        </div> <!-- end Panel -->
                        </div>
                    </div> <!-- end card-box -->
                </div> <!-- end col -->
            </div>
        </div>
    </div>
</div>