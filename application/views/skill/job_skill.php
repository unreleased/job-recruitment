

<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b>Skill</b></h4>
                        <div class="p-20">
                            <?php $this->load->view('session_msg'); ?>
                            <div class="row">

                            <div class="col-sm-12">

                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="m-b-30">
                                                 <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#exampleModal"><b>Add</b>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>SL</th>
                                                <th>Skill</th>
                                                <th>Category</th>
                                                <th>Action</th>

                                            </tr>
                                        </thead>


                                        <tbody>
                                           <?php $sl=0; foreach ($job_skill as $skill) {
    $sl++; ?>
                                           <tr id="<?php echo $skill['id']; ?>">
                                                <td><?php echo $sl; ?></td>
                                                <td id="skill_name"><?php echo $skill['name']; ?></td>
                                                <input id="category_id" type="hidden" value="<?php echo $skill['category_id'] ?>">
                                                <td id="category_name"><?php echo $skill['category_name']; ?></td>
                                                <td class="modalOpen">                                                   

                                                    <a data-toggle="modal" data-target="#exampleModal2" href="javascript:void(0)"><i style="margin-right:10px; font-size: 16px;" data-toggle="tooltip" title="Edit" class="fa fa-pencil"></i></a>

                                                    <a href="javascript:void(0)"><i data-id1="<?php echo $skill['id']; ?>" style="font-size: 16px;" data-toggle="tooltip" title="Delete" class="fa fa-trash btn_delete_skill"></i></a>                                                   

                                                </td>
                                            </tr>
                                           <?php
} ?> 
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end: page -->

                        </div> <!-- end Panel -->
                        </div>



        <!-- Modal Start -->                
        <div class="modal fade bs-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title header-title mt-0">Add New Skill</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">


                                    <form  role="form" action="<?=base_url('skill/view')?>" method="POST" >

                                        <div class="form-group row">
                                            <label for="skill" class="col-4 col-form-label">Job Category<span class="text-danger">*</span></label>
                                            <div class="col-7">
                                                <select class=" form-control select2 select2-hidden-accessible" name="category_id" tabindex="-1"  required="required">
                                                    <option value="">-Select a Category-</option>
                                                    <?php foreach ($job_category as $category) {
        ?>
                                                    <option value="<?php echo $category['id']; ?>">
                                                        <?php echo $category['name']; ?>    
                                                    </option>
                                                    <?php
    } ?>                                         
                                                </select>
                                            </div>
                                        </div>    

                                        <div class="form-group row">
                                            <label for="skill" class="col-4 col-form-label">Skill<span class="text-danger">*</span></label>
                                            <div class="col-7">
                                                <input type="text" required name="skill_name" class="form-control"
                                                placeholder="Enter Skill">
                                            </div>
                                        </div>

                            <div class="form-group row">
                                <div class="col-8 offset-4">

                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12 offset-4">
                                    <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">
                                        Save
                                    </button>

                                </div>
                            </div>
                            </form>

                            </div>
                            </div>



                            </div>
                            </div>
                            </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->





        <!-- Edit Modal Start -->                
        <div class="modal fade bs-example-modal-lg" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title header-title mt-0">Edit Skill</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">


                                    <form  role="form" action="<?=base_url('skill/update')?>" method="POST" >

                                        <div class="form-group row">
                                            <label for="skill" class="col-4 col-form-label">Job Category<span class="text-danger">*</span></label>
                                            <div class="col-7">
                                               <select class=" form-control select2 select2-hidden-accessible" name="category_id" tabindex="-1"  required="required" id="skill_edit_modal">                        
                                                </select>
                                            </div>
                                        </div>    

                                        <div class="form-group row">
                                            <label for="skill" class="col-4 col-form-label">Skill<span class="text-danger">*</span></label>
                                            <div class="col-7">
                                                <input type="text" id="e_skill_name" required name="skill_name" class="form-control"
                                                placeholder="Enter Skill">
                                            </div>
                                            <input type="hidden" id="e_skill_id" required name="skill_id" >
                                        </div>

                                        <div id="edit_div"></div>

                            <div class="form-group row">
                                <div class="col-8 offset-4">

                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12 offset-4">
                                    <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">
                                        Save
                                    </button>

                                </div>
                            </div>
                            </form>

                            </div>
                            </div>



                            </div>
                            </div>
                            </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->





                    </div> <!-- end card-box -->
                </div> <!-- end col -->
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(".select2").select2();
 $(document).ready(function() {
                $('form').parsley();
            });
</script>


<script type="text/javascript">
    $(document).ready(function() {
        //Buttons examples
        var table = $('#datatable-buttons').DataTable({
            lengthChange: true,
            lengthMenu: [25,50,100,150,200],          
        });       
    } );

</script>

<script>
    $('.modalOpen').on('click', function(){
        var skill_id= $(this).closest('tr').attr('id');
        var skill_name= $(this).parent('tr').find('#skill_name').text();
        var category_name= $(this).parent('tr').find('#category_name').text();
        var category_id= $(this).parent('tr').find('#category_id').val();
        $("#e_skill_name").val(skill_name);
        $("#e_category_name").val(category_name);
        $("#e_skill_id").val(skill_id);
        
        $.ajax({
            url:'SkillController/render_job_category',
            method:'POST',
            data:{id:category_id},

            success:function(data){
                $('#skill_edit_modal').html(data);
            }
        })


    });
</script>

<!-- delete skill -->
   <script type="text/javascript">
        $(document).on('click', '.btn_delete_skill', function(){         
         var id=$(this).data("id1");
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"skill/delete",  
             method:"post",  
             data:{id:id},
             dataType:"text",  
             success:function(data){  
                      $("#"+id).fadeOut(); 
                    }  
            });  
          }  
        }); 
      </script>




