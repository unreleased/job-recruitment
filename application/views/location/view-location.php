<!-- Required datatable js -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/plugins/datatables/buttons.colVis.min.js"></script> 

<div class="content-page">
    <div class="content">
        <div class="container-fluid">

    <div class="row">
        <div class="col-12">

          <div class="card-box table-responsive">
            <h4 class="m-t-0 header-title"><a href="add-location"><button>ADD LOCATION</button></a></h4>
                <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>Serial</th>
                      <th>City</th>
                      <th>State</th>
                      <th>Zip Code</th>
                      <th>Country</th>
                      
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <?php foreach ($location as $data):?>
                  <tbody>
                    

                    <td><?php if (isset($data['street'])) {
    echo $data['street'];
}?></td>
                    <td><?php if (isset($data['city'])) {
    echo $data['city'];
}?></td>
                    <td><?php if (isset($data['state'])) {
    echo $data['state'];
}?></td>
                    <td><?php if (isset($data['zip_code'])) {
    echo $data['zip_code'];
}?></td>
                    <td><?php if (isset($data['country'])) {
    echo $data['country'];
}?></td>
                    <td><?php if (isset($data['street'])) {
    echo $data['street'];
}?></td>
                
                </tbody>
                <?php endforeach;?>
            </table>
          </div>
        </div>
    </div> <!-- end row -->
</div><br><br><br><br>



    </div>
</div>







<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKIKDERJHh8CQ9wJYGcV7VVhUD6yOcYmM&libraries=places"></script>
<script type="text/javascript">
    var input = document.getElementById('street');
    console.log(input);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.addListener('place_changed', function() {

        var place = autocomplete.getPlace();
        //console.log(place.formatted_address);
        var data=place.formatted_address;

        if (!place.geometry) {

            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        var address = '';
        if (place.address_components) {
            console.log(place.address_components);
//alert(place.address_components);

            //document.getElementById('locName').value=place.name;
            document.getElementById('zip_code').value='';
            document.getElementById('state').value='';
            document.getElementById('city').value='';
            document.getElementById('country').value='';
//alert(place.address_components.length);
            for (var i = 0; i < place.address_components.length; i++) {
                for (var j = 0; j < place.address_components[i].types.length; j++) {
                    //alert(place.address_components[i].long_name);
                    if (place.address_components[i].types[j] == "postal_code") {
                        document.getElementById('zip_code').value = place.address_components[i].long_name;
                        break;
                    }else if (place.address_components[i].types[j]=="locality"){
                        document.getElementById('city').value=place.address_components[i].long_name;
                        //alert(place.address_components[i].long_name);
                        break;
                    }else if (place.address_components[i].types[j]=="administrative_area_level_2"){
                        document.getElementById('state').value=place.address_components[i].long_name;
                        break;
                    }else if (place.address_components[i].types[j]=="country"){
                        document.getElementById('country').value=place.address_components[i].long_name;
                        break;
                    }
                }
            }

        }

        $.ajax({
            type:'ajax',
            method:'get',
            url:'LocationController/getLatLong',
            data:{data:data},
            dataType:'json',
            success:function (res) {
                var htm='';
                htm+="<input type='hidden' name='latitude' id='latitude' value='"+res.latitude+"'>";
                htm+="<input type='hidden' name='longitude' id='longitude' value='"+res.longitude+"'>";
                $("#append").html(htm);

            }
        });
       
    });
</script>

<script>
function myFunction(value) {
    //alert("The input value has changed. The new value is: " + value);
    
    $('#latitude').val('');
     $('#longitude').val('');

}
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();

//Buttons examples
var table = $('#datatable-buttons').DataTable({
    lengthChange: true,
    lengthMenu: [25,50,100,150,200],
    buttons: [
    {
        extend: 'copyHtml5',
        exportOptions: {
            columns: [ 0,1,2,3 ]
        }
    },
    {
        extend: 'excelHtml5',
        exportOptions: {                                
            columns: [ 0,1,2,3]
        }
    },
    {
        extend: 'pdfHtml5',
        exportOptions: {
            columns: [ 0,1,2,3 ]
        }
    },

    ]
});

table.buttons().container()
.appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
} );

</script>
