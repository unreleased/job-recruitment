<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b>ADD LOCATION</b></h4>
                        <div class="p-20">
                            <form role="form" class="form-horizontal" method="post" action="add-location">
                                <div class="form-group row">
                                    <label class="col-2 col-form-label" >Street Address</label>
                                    <div class="col-10">
                                        <input type="text" id="street" name="street_address" class="form-control" placeholder="" onkeyup="myFunction(this.value)">
                                    </div>
                                </div>
                                <div id="append"></div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Zip Code</label>
                                    <div class="col-10">
                                        <input type="text" class="form-control" id="zip_code"name="zip_code" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">City</label>
                                    <div class="col-10">
                                        <input type="text" class="form-control" id="city" name="city" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">State</label>
                                    <div class="col-10">
                                        <input type="text" class="form-control" id="state" name="state" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Country</label>
                                    <div class="col-10">
                                        <input type="text" class="form-control" id="country" name="country" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-default">Save</button>
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                    </div> <!-- end card-box -->
                </div> <!-- end col -->
            </div>
        </div>
    </div>
</div>    