<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b>ADD NEW JOB</b></h4>
                        

                        <div class="p-20">
                            <form role="form" class="form-horizontal" method="POST" action="job/add" id="jobAddForm">
                                <div class="form-group row">
                                    <label class="col-2 col-form-label" for="example-input-small">Job Title</label>
                                    <div class="col-10">
                                        <input type="text" id="example-input-large" name="jobTitle" class="form-control input-lg" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-2 col-form-label" for="example-input-normal">Job Nature</label>
                                    <div class="col-10">
                                        <select class="form-control" name="jobNature">
                                            <option value="1">Part Time</option>
                                            <option value="2">Fulltime</option>
                                            <option value="3">Remote</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label" for="example-input-normal">Job Location</label>
                                    <div class="col-10">
                                        <select class="form-control" name="jobLocation">
                                            <?php echo $locations; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label" for="example-input-normal">Status</label>
                                    <div class="col-10">
                                        <select class="form-control" name="jobStatus">
                                            <option value="1">published</option>
                                            <option value="0">unpublished</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-2 col-form-label" for="example-input-large">Job Description</label>
                                    <div class="col-10">
                                        <textarea type="text" id="example-input-large" name="jobDescription" class="form-control input-lg summernote" placeholder=""></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Requirements</label>
                                    <div class="col-10">
                                        <textarea type="text" class="form-control summernote" placeholder="" name="jobRequirements"> </textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Job Skills</label>
                                    <div class="col-10">
                                        <!-- <input type="text" class="form-control" data-role="tagsinput" placeholder="" name="jobSkills[]"> -->
                                        <select name="jobSkills[]" id="jobSkills" class="form-control select2" multiple="multiple">
                                            <?php echo $skills; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Vacancy</label>
                                    <div class="col-10">
                                        <input type="number" min="1" class="form-control" placeholder="" name="jobVacancy">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Deadline</label>
                                    <div class="col-10">
                                        <input type="text" class="form-control" placeholder="" name="deadline" id="datepicker">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label"></label>
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-inverse waves-effect waves-light">Submit</button>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div> <!-- end card-box -->
                </div> <!-- end col -->
            </div>
        </div>
    </div>
</div>

<!-- ubold datepicker -->
<script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<!-- tags input -->
<script src="assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
<!-- select2 -->
<script src="assets/plugins/select2/js/select2.min.js"></script>
<script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<!--summernote init-->
<script src="assets/plugins/summernote/summernote.min.js"></script>
<!-- jquery validation -->
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>

<script>
    $('#datepicker').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    //select2 on job skills
    $('.select2').select2();

    //summernote on
    $(document).ready(function(){
            $('.summernote').summernote({
            height: 150,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: true                 // set focus to editable area after initializing summernote
        });
    });
    
    //add job form validation
    $('#jobAddForm').validate({
        rules:{
            jobTitle:{
                required:true
            },
            jobVacancy:{
                required:true,
                number:true,
                max:50,
            }
        },
        messages:{
            //field:message
        }
    })

</script>
