<?php

class JobModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Insert multiple instances
     * @param  string $tableName    Table name to insert data.
     * @param  array  $dataToInsert ex: arr =[ [1,2,3], [4,5,6] ] .
     *
     * @return void
     */
    public function insert($tableName, $dataToInsert)
    {
        $this->db
        ->insert_batch($tableName, $dataToInsert);

        return $this->db->insert_id();
    }
}
