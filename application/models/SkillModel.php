<?php
class SkillModel extends CI_Model
{
    public function insert($table_name, $data)
    {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function getAllData($tableName)
    {
        $this->db->select('*');
        $query = $this->db->get($tableName)->result_array();
        return $query;
    }

    public function getAllSkill()
    {
        $this->db->select('job_skill.*,job_category.name as category_name');
        $this->db->from('job_skill');
        $this->db->join('job_category', 'job_category.id=job_skill.category_id', 'LEFT');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function updateSkill($data)
    {
        $skill_id = $data['skill_id'];
        $this->db->set('category_id', $data['category_id']);
        $this->db->set('name', $data['name']);
        $this->db->where('id', $skill_id);
        return  $query=$this->db->update('job_skill');
    }

    public function checkSkill($name, $category_id)
    {
        $this->db->select('*');
        $this->db->from('job_skill');
        $this->db->where('name', $name);
        $this->db->where('category_id', $category_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function deleteJobSkill($id)
    {
        $this->db->where('id', $id);
        return $query = $this->db->delete('job_skill');
    }
}
