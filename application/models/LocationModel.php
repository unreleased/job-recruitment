<?php

class LocationModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data)
    {
        $this->db->insert('location', $data);
        return $this->db->insert_id();
    }

    public function select_where_one($table_name, $selector, $colom)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where($colom, $selector);
        return $this->db->get()->result_array();
    }

    public function select_where_two($table_name, $selectorOne, $colomOne, $selectorTwo, $colomTwo)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where($colomOne, $selectorOne);
        $this->db->where($colomTwo, $selectorTwo);
        return $this->db->get()->result_array();
    }

    public function select_where_lastdata($table_name, $selector, $colom)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where($colom, $selector);
        $this->db->order_by('created_at', 'desc');
        $this->db->limit(1);
        return $this->db->get()->result_array();
    }

    public function insertComment($table_name, $ins_data)
    {
        $this->db->insert($table_name, $ins_data);

        return $this->db->insert_id();
    }

    public function getAllUser($table_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Get all location recorded
     *
     * @return array all location array
     */
    public function all()
    {
        $res = $this->db
            ->select('*')
            ->get('job_locations')
            ->result_array();
        return $res;
    }
}
