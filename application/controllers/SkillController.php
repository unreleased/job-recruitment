<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SkillController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('SkillModel');
    }
    


    public function viewSkill()
    {
        $this->form_validation->set_rules('skill_name', 'Skill Name', 'required');
        $this->form_validation->set_rules('category_id', 'Job Category', 'required');
        if ($this->form_validation->run() == false) {
            $data['job_category'] 	= 	$this->SkillModel->getAllData('job_category');
            $data['job_skill'] 		= 	$this->SkillModel->getAllSkill('job_skill');
            $this->load->view('templates/header');
            $this->load->view('skill/job_skill', $data);
            $this->load->view('templates/sidebar');
            $this->load->view('templates/footer');
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $data['name'] 			= 	$clean['skill_name'];
            $data['category_id']	= 	$clean['category_id'];

            $checkResult = $this->SkillModel->checkSkill($data['name'], $data['category_id']);
            if (count($checkResult)>0) {
                $this->session->set_flashdata('error_msg', 'This skill already exist for this category');
                redirect('skill/view');
            }

            $result = $this->SkillModel->insert('job_skill', $data);
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Skill Added Succesfully');
                redirect('skill/view');
            }
        }
    }



    public function updateSkill()
    {
        $this->form_validation->set_rules('skill_name', 'Skill Name', 'required');
        $this->form_validation->set_rules('category_id', 'Job Category', 'required');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error_msg', 'Something May Wrong');
            redirect('skill/view');
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $data['name'] 			= 	$clean['skill_name'];
            $data['category_id']	= 	$clean['category_id'];
            $data['skill_id']		= 	$clean['skill_id'];

            $checkResult = $this->SkillModel->checkSkill($data['name'], $data['category_id']);
            if (count($checkResult)>0) {
                $this->session->set_flashdata('error_msg', 'This skill already exist for this category');
                redirect('skill/view');
            }

            $result = $this->SkillModel->updateSkill($data);
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Skill Updated Succesfully');
                redirect('skill/view');
            }
        }
    }


    public function render_job_category()
    {
        $post = $this->input->post();
        $job_category 	= 	$this->SkillModel->getAllData('job_category');
        $category_id 	= $post['id'];
        $html = '';
        foreach ($job_category as $category) {
            $selected=($category['id']==$category_id)?'selected':'';
            $html.='<option value="'.$category['id'].'"'.$selected.'>'.$category['name'].'</option>';
        }
        echo $html;
    }

    public function deleteSkill()
    {
        $id = $this->input->post('id');
        $result = $this->SkillModel->deleteJobSkill($id);
        if (count($result)>0) {
            echo "Deleted Successfully";
        }
    }
}
