<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AuthController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AuthModel');
        $this->load->library('form_validation');
    }

    
    
    public function login()
    {
        $loggedUserId = $this->session->userdata('id');
        if (isset($loggedUserId)) {
            redirect('dashboard');
        }
        $this->load->view('auth/login');
    }


    //////////////////////
    //admin site login //
    /////////////////////
    public function check_login()
    {
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error_msg', 'The login was unsucessful');
            redirect('login');
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $clean['password']=$this->encryptIt($clean['password']);
            $userInfo = $this->AuthModel->checkLogin($clean);
            if ($userInfo) {
                foreach ($userInfo as $key => $val) {
                    $this->session->set_userdata($key, $val);
                }
                redirect('dashboard');
            } else {
                $this->session->set_flashdata('error_msg', 'Incorrect Email or Password!');
                redirect('login');
            }
        }
    }

    public function decryptIt($q)
    {
        $cryptKey = 'Rakra8S90L50u65p27i10B';
        $qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
        return($qDecoded);
    }

    public function encryptIt($q)
    {
        $cryptKey = 'Rakra8S90L50u65p27i10B';
        $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
        return($qEncoded);
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }


    //////////////////////
    //Forgot Passowrd ///
    /////////////////////
    // public function forgot_password()
    // {
    // 	$this->load->view('auth/forgot_password');

    // }
    //edit by arif
    public function chk()
    {
    }
}
