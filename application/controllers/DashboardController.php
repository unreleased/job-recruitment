<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DashboardController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //$this->load->model('AuthModel');
        $loggedUserId = $this->session->userdata('id');
        if (!isset($loggedUserId)) {
            redirect('login');
        }
    }

    
    
    public function index()
    {
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('dashboard/dashboard');
        $this->load->view('templates/footer');
    }
}
