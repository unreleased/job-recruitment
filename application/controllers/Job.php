<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Job extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('JobModel');
        $this->load->model('LocationModel');
        $this->load->model('SkillModel');
    }

    public function index()
    {
        $this->load->view('templates/header');
        $this->load->view('job/add_job');
        $this->load->view('templates/sidebar');
        $this->load->view('templates/footer');
    }


    public function addJob()
    {
        $post         = $this->input->post();
        $clean        = $this->security->xss_clean($post);
        $dataToInsert = [];

        $this->form_validation->set_rules('jobTitle', 'Job Title', 'required');
        $this->form_validation->set_rules('jobLocation', 'Job Location', 'required');
        
        if ($this->form_validation->run() == false) {
            $data['locations'] = $this->renderLocations();
            $data['skills']    = $this->renderSkills();
            $this->load->view('templates/header');
            $this->load->view('job/add_job', $data);
            $this->load->view('templates/sidebar');
            $this->load->view('templates/footer');
        } else {
            $dataToInsert[] = [
            'title'          => $clean['jobTitle'],
            'description'    => $clean['jobDescription'],
            'requirements'   => $clean['jobRequirements'],
            'job_nature'     => $clean['jobNature'],
            'location_id'    => $clean['jobLocation'],
            'category_id'    => $clean['jobStatus'],
            'skills'         => isset($clean['jobSkills']) ? json_encode($clean['jobSkills']) : '',
            'num_of_vacancy' => $clean['jobVacancy'],
            'deadline'       => $clean['deadline'],
            'status'         => $clean['jobStatus'],
            'created_at'     =>date('Y-m-d'),
            'updated_at'     =>date('Y-m-d'),
            ];

            $insertId = $this->JobModel->insert('job', $dataToInsert);
            if ($insertId) {
                $this->session->set_flashdata('success_msg', 'Job added successfully');
            } else {
                $this->session->set_flashdata('success_msg', 'Something is wrong!');
            }
            redirect('dashboard');
        }
    }

    
    /**
     * Render all locations with option tag
     *
     * @param array $selected used for edit job to keep locations selected.
     *
     * @return string         options
     */
    public function renderLocations($selected = [])
    {
        $locations = $this->LocationModel->all();
        $option = '';
        foreach ($locations as $location) {
            $option .= '<option value="'.$location['id'].'">'. $location['location'].'</option>';
        }
        return $option;
    }


    /**
     * Render all skills with option tag
     *
     * @param array  $selected used for edit job to keep skills selected.
     *
     * @return string           options
     */
    public function renderSkills($selected = [])
    {
        $option = '';
        $skills = $this->SkillModel->getAllSkill();
        foreach ($skills as $skill) {
            $option .= '<option value="'.$skill['id'].'">'.$skill['name'].'</option>';
        }
        return $option;
    }
}
