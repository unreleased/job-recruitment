<?php
defined('BASEPATH') or exit('No direct script access allowed');

class EmailTemplateController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('EmailtemplateModel');
    }

    public function viewTemplates()
    {
        $data['email_templates'] = $this->EmailtemplateModel->getEmailTemplates();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view("emailtemplate/template_list", $data);
        $this->load->view('templates/footer');
    }

   

    public function editEmailTemplate($id)
    {
        $template_id           =   $id;
        $data['template']      =   $this->EmailtemplateModel->getEmailTemplate($template_id);
        $data['form_action']   =   'email_template_module/update_email_template';
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view("emailtemplate/template_edit_form", $data);
        $this->load->view('templates/footer');
    }

    public function updateEmailTemplate()
    {
        $template_id = $this->input->post('email_template_id');
        $data['email_template_subject']   =  trim($this->input->post('email_template_subject'));
        $data['email_template']           =  $this->input->post('email_template');

        $this->form_validation->set_rules('email_template_subject', 'Email Template Subject', 'required');
        $this->form_validation->set_rules('email_template', 'Email Template', 'required');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error_msg', validation_errors());
            redirect('emailtemplate/edit/'.$template_id);
        }

        $this->EmailtemplateModel->updateEmailTemplate($data, $template_id);
        $this->session->set_flashdata('success_msg', 'Update Successfully');
        redirect('emailtemplate/edit/'.$template_id);
    }
}
