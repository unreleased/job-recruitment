<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LocationController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('JobModel');
        $this->load->model('LocationModel');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['location']=$this->LocationModel->getAllUser('location');
        $this->load->view('templates/header');
        $this->load->view('location/view-location', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('templates/footer');
    }

    public function AddLocation()
    {
        $post=$this->input->post();
        if (!$post) {
            $this->load->view('templates/header');
            $this->load->view('location/add-location');
            $this->load->view('templates/sidebar');
            $this->load->view('templates/footer');
        } else {
            $clean=$this->security->xss_clean($post);
            $ins_id=$this->LocationModel->insert($clean);
            if ($ins_id) {
                redirect('job-location');
            }
        }
    }

    public function viewLocation()
    {
        $data['location']=$this->LocationModel->getAllUser('location');
        print_r($data);
        die;
        $this->load->view('templates/header');
        $this->load->view('location/view-location', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('templates/footer');
    }

    public function getLatLong()
    {
        //echo "ok";die;
        $address =$this->input->get('data');
        
        if (!empty($address)) {
            //Formatted address
            $formattedAddr = str_replace(' ', '+', $address);
            //Send request and receive json data by address
            $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false&key=AIzaSyDSISiLvEGKT6zI1-D-36T3pWYRajSn-dI');
            $output = json_decode($geocodeFromAddr);
            //Get latitude and longitute from json data
            // echo "<pre>"; print_r($output);die;
            $data['latitude']  = $output->results[0]->geometry->location->lat;
            $data['longitude'] = $output->results[0]->geometry->location->lng;
            //Return latitude and longitude of the given address
            //echo "<pre>";print_r($data);die;
            echo json_encode($data);
        }
    }

    //Categories view add edit delete
    public function viewCategories()
    {
        $data['categories']=$this->LocationModel->getAllUser('job_category');
        $this->load->view('templates/header');
        $this->load->view('categories/view-categories', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('templates/footer');
    }

    public function addCategories()
    {
        $post=$this->input->post();
        if (!$post) {
            redirect('categories/view');
        } else {
            $clean=$this->security->xss_clean($post);
            $ins_id=$this->LocationModel->insert('job_category', $clean);
            if ($ins_id) {
                redirect('categories/view');
            }
        }
    }
}
